from lce.exceptions import ValidationError
from lce.graph import parse_graph
import pytest
import re


def test_parse_simple():
    spec = {
        "operators": [
            {
                "type": "LoadSamplePoints",
                "parameters": {
                    "points": [
                        [0, 0, 0],
                    ],
                },
            },
            {
                "type": "SampleBox",
                "parameters": {
                    "size": [1, 1, 1],
                },
            },
            {
                "type": "Threshold",
                "parameters": {
                    "width": 1.0,
                },
            },
        ],
        "edges": [
            {
                "source": 0,
                "target": [1, "Points"],
            },
            {
                "source": 1,
                "target": [2, "Samples"],
            },
        ],
    }
    graph = parse_graph(spec, expected_output_count=1)
    assert graph.number_of_nodes() == 3
    assert graph.number_of_edges() == 2
    assert graph.has_edge(0, 1, "Points")
    assert graph.has_edge(1, 2, "Samples")


def test_parse_cycle():
    spec = {
        "operators": [
            {
                "type": "LoadSamplePoints",
                "parameters": {
                    "points": [
                        [0, 0, 0],
                    ],
                },
            },
            {
                "type": "SampleSurfaceLattice",
                "parameters": {
                    "size": [1, 1, 1],
                },
            },
            {
                "type": "Shell",
                "parameters": {
                    "thickness": 1.0,
                },
            },
            {
                "type": "Threshold",
                "parameters": {
                    "width": 1.0,
                },
            },
        ],
        "edges": [
            {
                "source": 0,
                "target": [1, "Points"],
            },
            {
                "source": 1,
                "target": [2, "Samples"],
            },
            {
                "source": 2,
                "target": [1, "ScaleGrading"],
            },
            {
                "source": 2,
                "target": [3, "Samples"],
            },
        ],
    }
    substr = re.escape(r"Graph is not a directed acyclic graph (DAG)")
    with pytest.raises(ValidationError, match=substr):
        graph = parse_graph(spec)


def test_parse_validate_output_count():
    spec = {
        "operators": [
            {
                "type": "LoadSamplePoints",
                "parameters": {
                    "points": [
                        [0, 0, 0],
                    ],
                },
            },
            {
                "type": "SampleBox",
                "parameters": {
                    "size": [1, 1, 1],
                },
            },
            {
                "type": "Threshold",
                "parameters": {
                    "width": 1.0,
                },
            },
            {
                "type": "Threshold",
                "parameters": {
                    "width": 1.0,
                },
            },
        ],
        "edges": [
            {
                "source": 0,
                "target": [1, "Points"],
            },
            {
                "source": 1,
                "target": [2, "Samples"],
            },
            {
                "source": 1,
                "target": [3, "Samples"],
            },
        ],
    }
    substr = re.escape(r"Graph has 2 outputs: '2' (Threshold), '3' (Threshold)")
    with pytest.raises(ValidationError, match=substr):
        graph = parse_graph(spec, expected_output_count=1)


def test_parse_existing_input():
    spec = {
        "operators": [
            {
                "type": "LoadSamplePoints",
                "parameters": {
                    "points": [
                        [0, 0, 0],
                    ],
                },
            },
            {
                "type": "LoadSamplePoints",
                "parameters": {
                    "points": [
                        [0, 0, 0],
                    ],
                },
            },
            {
                "type": "SampleBox",
                "parameters": {
                    "size": [1, 1, 1],
                },
            },
        ],
        "edges": [
            {
                "source": 0,
                "target": [2, "Points"],
            },
            {
                "source": 1,
                "target": [2, "Points"],
            },
        ],
    }
    substr = re.escape(
        r"edge '1': connection to target already exists: "
        r"source '0' (LoadSamplePoints) → "
        r"target '2' (SampleBox) Points"
    )
    with pytest.raises(ValidationError, match=substr):
        graph = parse_graph(spec)


def test_parse_diamond():
    spec = {
        "operators": [
            {
                "type": "LoadSamplePoints",
                "parameters": {
                    "points": [
                        [0, 0, 0],
                    ],
                },
            },
            {
                "type": "SampleBox",
                "parameters": {
                    "size": [1, 1, 1],
                },
            },
            {
                "type": "SampleSurfaceLattice",
                "parameters": {
                    "lattice_type": "Gyroid",
                },
            },
            {
                "type": "Threshold",
                "parameters": {
                    "width": 1.0,
                },
            },
        ],
        "edges": [
            {
                "source": 0,
                "target": [1, "Points"],
            },
            {
                "source": 0,
                "target": [2, "Points"],
            },
            {
                "source": 1,
                "target": [2, "ScaleGrading"],
            },
            {
                "source": 1,
                "target": [2, "ThresholdGrading"],
            },
            {
                "source": 2,
                "target": [3, "Samples"],
            },
        ],
    }
    graph = parse_graph(spec)
    assert graph.number_of_nodes() == 4
    assert graph.number_of_edges() == 5
    assert graph.has_edge(0, 1, "Points")
    assert graph.has_edge(0, 2, "Points")
    assert graph.has_edge(1, 2, "ScaleGrading")
    assert graph.has_edge(1, 2, "ThresholdGrading")
    assert graph.has_edge(2, 3, "Samples")


def test_parse_invalid_type():
    spec = {
        "operators": [
            {
                "type": "DoesNotExist",
            }
        ],
    }
    with pytest.raises(KeyError):
        graph = parse_graph(spec)


def test_parse_invalid_edge_source():
    spec = {
        "operators": [
            {
                "type": "LoadSamplePoints",
                "parameters": {
                    "points": [
                        [0, 0, 0],
                    ],
                },
            },
            {
                "type": "SampleBox",
                "parameters": {
                    "size": [1, 1, 1],
                },
            },
        ],
        "edges": [
            {
                "source": 2,
                "target": [1, "Points"],
            },
        ],
    }
    with pytest.raises(
        ValidationError,
        match=r"edge '0': source '2' does not exist",
    ):
        graph = parse_graph(spec)


def test_parse_invalid_edge_target():
    spec = {
        "operators": [
            {
                "type": "LoadSamplePoints",
                "parameters": {
                    "points": [
                        [0, 0, 0],
                    ],
                },
            },
            {
                "type": "SampleBox",
                "parameters": {
                    "size": [1, 1, 1],
                },
            },
        ],
        "edges": [
            {
                "source": 0,
                "target": [2, "Points"],
            },
        ],
    }
    with pytest.raises(
        ValidationError,
        match=r"edge '0': target '2' does not exist",
    ):
        graph = parse_graph(spec)


def test_parse_invalid_edge_target_input():
    spec = {
        "operators": [
            {
                "type": "LoadSamplePoints",
                "parameters": {
                    "points": [
                        [0, 0, 0],
                    ],
                },
            },
            {
                "type": "SampleBox",
                "parameters": {
                    "size": [1, 1, 1],
                },
            },
        ],
        "edges": [
            {
                "source": 0,
                "target": [1, "Samples"],
            },
        ],
    }
    substr = re.escape(r"edge '0': target '1' (SampleBox) has no input 'Samples'")
    with pytest.raises(ValidationError, match=substr):
        graph = parse_graph(spec)


def test_parse_invalid_edge_type():
    spec = {
        "operators": [
            {
                "type": "LoadSamplePoints",
                "parameters": {
                    "points": [
                        [0, 0, 0],
                    ],
                },
            },
            {
                "type": "Threshold",
                "parameters": {
                    "width": 1.0,
                },
            },
        ],
        "edges": [
            {
                "source": 0,
                "target": [1, "Samples"],
            },
        ],
    }

    substr = re.escape(
        r"edge '0': types do not match: "
        r"source '0' (LoadSamplePoints) [vec3f] → "
        r"target '1' (Threshold) Samples [float]"
    )
    with pytest.raises(ValidationError, match=substr):
        graph = parse_graph(spec)
