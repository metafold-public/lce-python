from lce.graph import dump_graph, parse_graph


def test_dump_graph():
    spec = {
        "operators": [
            {
                "type": "LoadSamplePoints",
                "parameters": {
                    "points": [
                        [0, 0, 0],
                    ],
                },
            },
            {
                "type": "SampleBox",
                "parameters": {
                    "size": [1, 1, 1],
                },
            },
            {
                "type": "Threshold",
                "parameters": {
                    "width": 1.0,
                },
            },
        ],
        "edges": [
            {
                "source": 0,
                "target": [1, "Points"],
            },
            {
                "source": 1,
                "target": [2, "Samples"],
            },
        ],
    }
    graph = parse_graph(spec)
    assert dump_graph(graph) == spec


def test_dump_edited_graph():
    spec = {
        "operators": [
            {
                "type": "LoadSamplePoints",
                "parameters": {
                    "points": [
                        [0, 0, 0],
                    ],
                },
            },
            {
                "type": "SampleBox",
                "parameters": {
                    "size": [1, 1, 1],
                },
            },
            {
                "type": "Threshold",
                "parameters": {
                    "width": 1.0,
                },
            },
        ],
        "edges": [
            {
                "source": 0,
                "target": [1, "Points"],
            },
            {
                "source": 1,
                "target": [2, "Samples"],
            },
        ],
    }

    graph = parse_graph(spec)
    graph.nodes[0]["parameters"]["points"][0] = [1, 2, 3]
    spec["operators"][0]["parameters"]["points"][0] = [1, 2, 3]
    assert dump_graph(graph) == spec
