# Lightcycle Engine Python

Python implementation of the Lightcycle graph specification.

This package is part of the Lightcycle Software Development Kit (SDK), available
to clients on license tiers with API access.

Lightcycle graphs are encoded as JSON documents.

:construction: Add link to JSON schema

Read more about the graph specification in our [documentation](#).

## Getting Started

### Installation

```
pip install git+https://gitlab.com/metafold-public/lce-python@1.10.1
```

### API

At the moment the package includes a handful of simple utility functions for
parsing (and validating) Lightcycle graphs into [NetworkX][] [MultiDiGraphs][],
and dumping them back out to a JSON-serializable dictionary.

- `lce.graph.parse_graph`
- `lce.graph.dump_graph`

[NetworkX]: https://networkx.org/
[MultiDiGraphs]: https://networkx.org/documentation/stable/reference/classes/multidigraph.html
