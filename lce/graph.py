from collections import namedtuple
from enum import Enum
from lce.exceptions import ValidationError
from networkx import MultiDiGraph
from os import PathLike
from textwrap import indent
from typing import Any, IO, Iterator, Union
import json
import networkx as nx


class ComponentType(Enum):
    """Types of components in operator buffers."""
    NONE = "none"
    BYTE = "byte"
    INTEGER = "integer"
    FLOAT = "float"
    VEC2I = "vec2i"
    VEC2F = "vec2f"
    VEC3I = "vec3i"
    VEC3F = "vec3f"
    VEC4I = "vec4i"
    VEC4F = "vec4f"
    MAT2F = "mat2f"
    MAT3F = "mat3f"
    MAT4F = "mat4f"


Input = namedtuple("Input", ["type", "optional"], defaults=[None, False])
"""Operator input definition."""


Operator = namedtuple(
    "Operator",
    ["output", "inputs", "assets"], defaults=[None, dict(), tuple()]
)
"""Operator definition."""


_definitions = {
    "ComputeNormals": Operator(
        output=ComponentType.VEC3F,
        inputs={
            "Points": Input(type=ComponentType.VEC3F),
            "Volume": Input(type=ComponentType.FLOAT),
        },
    ),
    "ComputeCurvatures": Operator(
        output=ComponentType.VEC2F,
        inputs={
            "Samples": Input(type=ComponentType.FLOAT),
        },
    ),
    "Contour": Operator(
        output=ComponentType.VEC3F,
        inputs={
            "Points": Input(type=ComponentType.VEC3F),
        },
    ),
    "CSG": Operator(
        output=ComponentType.FLOAT,
        inputs={
            "A": Input(type=ComponentType.FLOAT),
            "B": Input(type=ComponentType.FLOAT),
        },
    ),
    "DistanceTransform": Operator(
        output=ComponentType.FLOAT,
        inputs={
            "Samples": Input(type=ComponentType.FLOAT),
        },
    ),
    "GenerateSamplePoints": Operator(
        output=ComponentType.VEC3F,
    ),
    "GradeCellSize": Operator(
        output=ComponentType.VEC3F,
        inputs={
            "Points": Input(type=ComponentType.VEC3F),
        },
    ),
    "InterpolateBoundaryCoords": Operator(
        output=ComponentType.VEC3F,
        inputs={
            "Points": Input(type=ComponentType.VEC3F),
        },
        assets=["mesh_data", "boundary_data"],
    ),
    "LinearFilter": Operator(
        output=ComponentType.FLOAT,
        inputs={
            "InputA": Input(type=ComponentType.FLOAT),
            "InputB": Input(type=ComponentType.FLOAT),
        },
    ),
    "LoadSamplePoints": Operator(
        output=ComponentType.VEC3F,
    ),
    "LoadVolume": Operator(
        assets=["volume_data"],
    ),
    "MapTexturePrimitive": Operator(
        output=ComponentType.FLOAT,
        inputs={
            "Points": Input(type=ComponentType.VEC3F),
            "Volume": Input(type=ComponentType.FLOAT),
            "Image": Input(type=ComponentType.FLOAT),
        },
    ),
    "Redistance": Operator(
        output=ComponentType.FLOAT,
        inputs={
            "Samples": Input(type=ComponentType.FLOAT),
        },
    ),
    "SampleBeam": Operator(
        output=ComponentType.FLOAT,
        inputs={
            "Points": Input(type=ComponentType.VEC3F),
        },
        assets=["network_data", "bvh_data"],
    ),
    "SampleBox": Operator(
        output=ComponentType.FLOAT,
        inputs={
            "Points": Input(type=ComponentType.VEC3F),
        },
    ),
    "SampleLattice": Operator(
        output=ComponentType.FLOAT,
        inputs={
            "Points": Input(type=ComponentType.VEC3F),
            "ScaleGrading": Input(type=ComponentType.FLOAT, optional=True),
            "SectionRadiusGrading": Input(type=ComponentType.FLOAT, optional=True),
        },
    ),
    "SampleCustomShape": Operator(
        output=ComponentType.FLOAT,
        inputs={
            "Points": Input(type=ComponentType.VEC3F),
        },
        assets=["shader_data"],
    ),
    "SampleSpinodoid": Operator(
        output=ComponentType.FLOAT,
        inputs={
            "Points": Input(type=ComponentType.VEC3F),
        },
    ),
    "SampleSurfaceLattice": Operator(
        output=ComponentType.FLOAT,
        inputs={
            "Points": Input(type=ComponentType.VEC3F),
            "ScaleGrading": Input(type=ComponentType.FLOAT, optional=True),
            "ThresholdGrading": Input(type=ComponentType.FLOAT, optional=True),
        },
    ),
    "SampleTriangleMesh": Operator(
        output=ComponentType.FLOAT,
        inputs={
            "Points": Input(type=ComponentType.VEC3F),
        },
        assets=["mesh_data"],
    ),
    "SampleTriangleMeshBvh": Operator(
        output=ComponentType.FLOAT,
        inputs={
            "Points": Input(type=ComponentType.VEC3F),
        },
        assets=["mesh_data", "bvh_data"],
    ),
    "SampleVolume": Operator(
        inputs={
            "Points": Input(type=ComponentType.VEC3F),
            "Volume": Input(),
        },
    ),
    "Shell": Operator(
        output=ComponentType.FLOAT,
        inputs={
            "Samples": Input(type=ComponentType.FLOAT),
        },
    ),
    "Threshold": Operator(
        output=ComponentType.BYTE,
        inputs={
            "Samples": Input(type=ComponentType.FLOAT),
        },
    ),
    "TransformCylindricalCoords": Operator(
        output=ComponentType.VEC3F,
        inputs={
            "Points": Input(type=ComponentType.VEC3F),
        },
    ),
    "TransformMirrorCoords": Operator(
        output=ComponentType.VEC3F,
        inputs={
            "Points": Input(type=ComponentType.VEC3F),
        },
    ),
    "TransformSphericalCoords": Operator(
        output=ComponentType.VEC3F,
        inputs={
            "Points": Input(type=ComponentType.VEC3F),
        },
    ),
    "TransformTwistCoords": Operator(
        output=ComponentType.VEC3F,
        inputs={
            "Points": Input(type=ComponentType.VEC3F),
        },
    ),
}


def parse_graph(
    spec: dict,
    expected_output_count: int = 0,
) -> MultiDiGraph:
    """Parse the Lightcycle graph.

    Args:
        spec: Lightcycle graph.
        expected_output_count: Optional validation on number of graph outputs.
            Outputs are not checked if <= 0.

    Returns:
        Parsed graph as a NetworkX MultiDiGraph.

    Raises:
        ValidationError: If there are errors while parsing specification.
    """
    graph = MultiDiGraph()

    for i, operator in enumerate(spec["operators"]):
        defn = _definitions[operator["type"]]
        graph.add_node(i, definition=defn, **operator)

    errors = []
    for i, edge in enumerate(spec.get("edges", [])):
        source = edge["source"]
        target, input_name = edge["target"]

        if source not in graph.nodes:
            errors.append(f"edge '{i}': source '{source}' does not exist")
            continue

        source_type = graph.nodes[source]["definition"].output

        if target not in graph.nodes:
            errors.append(f"edge '{i}': target '{target}' does not exist")
            continue

        target_input = graph.nodes[target]["definition"].inputs.get(input_name)
        if not target_input:
            target_type_name = graph.nodes[target]["type"]
            errors.append(
                f"edge '{i}': target '{target}' ({target_type_name}) "
                f"has no input '{input_name}'"
            )
            continue

        if source_type and target_input.type and source_type != target_input.type:
            source_type_name = graph.nodes[source]["type"]
            target_type_name = graph.nodes[target]["type"]
            errors.append(
                f"edge '{i}': types do not match: "
                f"source '{source}' ({source_type_name}) [{source_type.value}] → "
                f"target '{target}' ({target_type_name}) {input_name} [{target_input.type.value}]"
            )
            continue

        # Ugly O(n) search to check for duplicate connections to the same input
        existing_source = -1
        for edge_source, _, edge_input_name in graph.in_edges(target, keys=True):
            if input_name == edge_input_name:
                existing_source = edge_source
                break

        if existing_source >= 0:
            source_type_name = graph.nodes[existing_source]["type"]
            target_type_name = graph.nodes[target]["type"]
            errors.append(
                f"edge '{i}': connection to target already exists: "
                f"source '{existing_source}' ({source_type_name}) → "
                f"target '{target}' ({target_type_name}) {input_name}"
            )
            continue

        graph.add_edge(source, target, key=input_name)

    if errors:
        msgs = "\n".join(indent(e, "  ") for e in errors)
        raise ValidationError("Graph validation failed:\n{}".format(msgs))

    if not nx.is_directed_acyclic_graph(graph):
        raise ValidationError(
            "Graph is not a directed acyclic graph (DAG), "
            "i.e. likely contains a cycle"
        )

    if expected_output_count > 0:
        outputs = []
        for node, degree in graph.out_degree(graph.nodes):
            if degree == 0:
                outputs.append(node)
        output_count = len(outputs)
        if output_count != expected_output_count:
            output_names = ", ".join(
                f"'{n}' ({graph.nodes[n]['type']})" for n in outputs
            )
            raise ValidationError(
                f"Graph has {output_count} outputs: {output_names}"
            )

    return graph


def parse_graph_json(
    path_or_file: Union[str, bytes, PathLike, IO[Any]],
    expected_output_count: int = 0,
) -> MultiDiGraph:
    """Parse the Lightcycle graph from file.

    Args:
        path_or_file: Path-like object or file-like object (must be opened for
            read in text-mode).
        expected_output_count: Optional validation on number of graph outputs.
            Outputs are not checked if <= 0.

    Returns:
        Parsed graph as a NetworkX MultiDiGraph.

    Raises:
        ValidationError: If there are errors while parsing specification.
    """
    spec: dict
    if isinstance(path_or_file, str | bytes | PathLike):
        with open(path_or_file, "r") as f:
            spec = json.load(f)
    else:
        spec = json.load(path_or_file)

    return parse_graph(spec)


def dump_graph(graph: MultiDiGraph) -> dict:
    """Dump the Lightcycle graph to a JSON-serializable dictionary."""
    operators = []
    for _, node in graph.nodes.items():
        operator = { "type": node["type"] }
        if parameters := node.get("parameters"):
            operator["parameters"] = parameters
        operators.append(operator)

    edges = []
    for source, target, input_name in graph.edges(keys=True):
        edges.append({
            "source": source,
            "target": [target, input_name],
        })

    return { "operators": operators, "edges": edges }


def iter_assets(graph: MultiDiGraph) -> Iterator[dict]:
    """Iterate over asset references in the Lightcycle graph."""
    for _, node in graph.nodes.items():
        for asset in node["definition"].assets:
            yield node["parameters"][asset]
