class ValidationError(Exception):
    """Raised when a Lightcycle graph is invalid.
    """
