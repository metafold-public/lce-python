from matplotlib import pyplot
from networkx import DiGraph
import networkx as nx


def draw_graph(graph: DiGraph, font_family: str = "Inter") -> None:
    """Draw the given Lightcycle graph in topological order.
    """
    for layer, nodes in enumerate(nx.topological_generations(graph)):
        # multipartite_layout expects the layer as a node attribute, so add the
        # numeric layer value as a node attribute.
        for node in nodes:
            graph.nodes[node]["layer"] = layer

    # Compute the multipartite_layout using the "layer" node attribute
    pos = nx.multipartite_layout(graph, subset_key="layer")

    fig, ax = pyplot.subplots()
    nx.draw_networkx(graph, pos=pos, with_labels=False, ax=ax)

    labels = dict((n, f"{n}: {graph.nodes[n]['type']}") for n in graph.nodes)
    # Offset labels slightly on vertical axis
    label_pos = dict((node, [p[0], p[1] + 0.02]) for node, p in pos.items())
    nx.draw_networkx_labels(
        graph,
        pos=label_pos,
        labels=labels,
        font_family=font_family,
        font_size=10,
        bbox=dict(facecolor="white", alpha=0.8),
        ax=ax,
    )

    edge_labels = dict((e, e[2]) for e in graph.edges)
    nx.draw_networkx_edge_labels(
        graph,
        pos=pos,
        edge_labels=edge_labels,
        font_family=font_family,
        label_pos=0.2,
        rotate=False,
        ax=ax
    )

    fig.tight_layout()
    pyplot.show()
