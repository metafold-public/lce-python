from argparse import ArgumentParser, FileType, Namespace
from lce.contrib.draw_graph import draw_graph
from lce.graph import parse_graph_json
import sys


def main() -> int:
    parser = ArgumentParser(description="LCE command-line utility")
    parser.set_defaults(func=lambda _: parser.print_usage())
    subparsers = parser.add_subparsers()

    help_ = subparsers.add_parser(
        "help", add_help=False, help="Print help for a subcommand"
    )

    graph_common = ArgumentParser(add_help=False)
    graph_common.add_argument(
        "spec", nargs="?", type=FileType("r"), default=sys.stdin
    )

    validate = subparsers.add_parser(
        "validate", parents=[graph_common], help="Validate the LCE graph"
    )
    validate.set_defaults(func=_validate)

    draw = subparsers.add_parser(
        "draw", parents=[graph_common], help="Draw the LCE graph"
    )
    draw.set_defaults(func=_draw)

    subcommands = list(subparsers.choices.keys())
    subcommands.pop(subcommands.index("help"))

    help_.add_argument("subcommand", type=str, choices=subcommands)

    def print_help(args: Namespace) -> None:
        subparser = subparsers.choices.get(args.subcommand, parser)
        subparser.print_help()

    help_.set_defaults(func=print_help)

    args = parser.parse_args()
    return args.func(args)


def _validate(args: Namespace) -> int:
    # Raises ValidationError if specification is invalid
    parse_graph_json(args.spec, expected_output_count=1)
    return 0


def _draw(args: Namespace) -> int:
    graph = parse_graph_json(args.spec)
    draw_graph(graph)
    return 0


if __name__ == "__main__":
    sys.exit(main())
